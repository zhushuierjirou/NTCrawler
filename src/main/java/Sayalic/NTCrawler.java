package Sayalic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.fluent.Request;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Sona on 8/31/16.
 */
public class NTCrawler {
    private static ObjectMapper mapper = new ObjectMapper();

    public static List<Company> crawlCompany(int page) {
        String url = String.format("http://www.neitui.me/?name=company&handle=index&keyword=&page=%d", page);
        try {
            String content = getUrlResponse(url);
            Document doc = Jsoup.parse(content);
            return doc.select("#companyIndex > div.maincontent > div > div.hot_tuiers > div > div > div.cont > div.jobtitle.clearfix > strong > a").stream()
                    .map(e -> "http://www.neitui.me" + e.attr("href"))
                    .map(NTCrawler::crawlCompany)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            System.out.println("Crawl " + url + " failed.");
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    private static String getUrlResponse(String url) throws IOException {
        return Request.Get(url).execute().returnContent().asString();
    }

    private static Company crawlCompany(String url) {
        Company company = new Company();
        try {
            String content = getUrlResponse(url);
            Document doc = Jsoup.parse(content);
            company.setName(doc.select("#J_Company_Default > h3 > span").text().split(" ")[0]);
            company.setDomain(doc.select("#J_Company_Default > h3 > span > em > a").text());
            company.setLocation(doc.select("#J_Company_Default > div.company_description.clearfix > div.company_description_l > div:nth-child(1) > span:nth-child(2)").text());
            company.setSize(doc.select("#J_Company_Default > div.company_description.clearfix > div.company_description_r > div:nth-child(1) > span:nth-child(2)").text());
            company.setFoundation(doc.select("#J_Company_Default > div.company_description.clearfix > div.company_description_r > div:nth-child(2) > span:nth-child(2)").text());
            company.setSlogan(doc.select("#J_Company_Default > div.company_vision > div > span:nth-child(2)").text());
            company.setIndustry(doc.select("#J_Company_Default > div.company_description.clearfix > div.company_description_l > div:nth-child(2) > span:nth-child(2)").text());
            company.setIntroduction(doc.select("#J_Company_Textarea").html());
            company.setTags(doc.select("#J_Company_Tags > ul > li").stream().map(Element::text).collect(Collectors.toList()));
            company.setUrl(url);
            company.setLogoUrl(doc.select("#J_Img_Uploader > img").attr("src"));
//            company.setJobs(doc.select("#companyDetail > div.maincontent > div.plate.userresume > div.content.place_list > ul > li > div.cont > div.jobnote.f15 > a").stream()
//                    .map(e -> "http://www.neitui.me" + e.attr("href"))
//                    .parallel()
//                    .map(NTCrawler::crawlJob)
//                    .collect(Collectors.toList()));
        } catch (Exception e) {
            System.out.println("Crawl " + url + " failed.");
            e.printStackTrace();
        }
        return company;
    }

    private static Job crawlJob(String url) {
        Job job = new Job();
        try {
            String content = getUrlResponse(url);
            Document doc = Jsoup.parse(content);
            job.setReferrerNick(doc.select("#detail > div > ul > li > div.cont > div:nth-child(1) > a.defaulta").text());
            job.setReferrerAvatarUrl(doc.select("#detail > div > ul > li > div.img > a.icon > img").attr("src"));
            job.setReferrerUrl("http://www.neitui.me" + doc.select("#detail > div > ul > li > div.cont > div:nth-child(1) > a.defaulta").attr("href"));
            job.setApartment(doc.select("#detail > div > ul > li > div.cont > div.jobtitle > span.jobtitle-l").text().split("：")[1]);
            NTUser user = crawlUser(job.getReferrerUrl());
            job.setReferrerName(user.getName());
            job.setReferrerTitle(user.getTitle());
            job.setAddress(doc.select("#detail > div > ul > li > div.cont > div.jobtitle > span.jobtitle-r").text().split("：")[1]);
            String salary = doc.select("#detail > div > ul > li > div.cont > div:nth-child(2) > span.padding-r10.pay").text();
            if (salary.length() >= 2) {
                job.setSalary(salary.substring(1, salary.length() - 1));
            }
            String experience = doc.select("#detail > div > ul > li > div.cont > div:nth-child(2) > span.padding-r10.experience").text();
            if (experience.length() >= 2) {
                job.setExperience(experience.substring(1, experience.length() - 1));
            }
            job.setTitle(doc.select("#detail > div > ul > li > div.cont > div:nth-child(2) > strong").text());
            job.setUrl(url);
            job.setTags(doc.select("#detail > div > ul > li > div.cont > div.company_tags_wrap > ul > li").stream().map(Element::text).collect(Collectors.toList()));
            job.setDescription(doc.select("#detail > div > ul > li > div.cont > div.jobdetail.nooverflow").text());
        } catch (Exception e) {
            System.out.println("Crawl " + url + " failed.");
            e.printStackTrace();
        }
        System.out.println(System.currentTimeMillis() / 1000 + " " + job.getApartment() + " " + job.getTitle());
        return job;
    }

    private static NTUser crawlUser(String url) {
        NTUser user = new NTUser();
        try {
            String content = Request.Get(url).execute().returnContent().asString();
            Document doc = Jsoup.parse(content);
            String nick = doc.select("#resumeLists > div.sider > div > div > div > div.det > h3 > a").text();
            String[] parts = doc.select("#resumeLists > div.sider > div > div > div > div.approve_info.clearfix > p").text().split(" ");
            String title = Stream.of(parts).limit(parts.length - 1).collect(Collectors.joining());
            String name = parts[parts.length - 1];
            String avatarUrl = doc.select("#resumeLists > div.sider > div > div > div > div.img > a:nth-child(1) > img").attr("src");
            user.setName(name);
            user.setTitle(title);
            user.setNick(nick);
            user.setAvatarUrl(avatarUrl);
        } catch (IOException e) {
            System.out.println("Crawl " + url + " failed.");
            e.printStackTrace();
        }
        return user;
    }

    private static String objectToJson(Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void main(String[] args) throws FileNotFoundException {
        PrintWriter printWriter = new PrintWriter("/tmp/nt.json");
        for (int i = 0; i < 100; i++) {
            System.out.println(System.currentTimeMillis() / 1000 + " " + i);
            crawlCompany(i).stream().map(NTCrawler::objectToJson).forEach(printWriter::println);
        }
        printWriter.close();
    }
}
