package Sayalic;

import org.apache.http.client.fluent.Request;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class App
{
    public static void main( String[] args ) throws FileNotFoundException {
        PrintWriter printWriter = new PrintWriter("/tmp/neitui.csv");
        int beginIndex = 922987;
        Logger logger = Logger.getLogger("app");
        for(int i = 0; i < 10; i++) {
            logger.info(String.valueOf(i));
            IntStream.range(0, 100).parallel().mapToObj(x -> String.format("http://www.neitui.me/?name=user&handle=detail&id=%d&uid=%d&page=1", beginIndex - x, beginIndex - x))
                    .map(App::crawl).flatMap(user -> userToCSVLines(user).stream()).forEach(printWriter::println);
        }
        printWriter.close();
    }

    private static List<String> userToCSVLines(NTUser user) {
        return user.getJobs().stream().map(job -> user.getNick() + "," + user.getName() + "," + user.getAvatarUrl() + ","
                + user.getTitle() + "," + job.getTitle() +  "," + job.getApartment() + "," + job.getAddress() + ","
                + job.getSalary()).collect(Collectors.toList());
    }

    private static NTUser crawl(String url) {
        NTUser user = new NTUser();
        user.setJobs(new ArrayList<>());
        try {
            String content = Request.Get(url).execute().returnContent().asString();
            if (content.contains("暂时没有内推职位！")) {
                return user;
            }
            Document doc = Jsoup.parse(content);
            String nick = doc.select("#resumeLists > div.sider > div > div > div > div.det > h3 > a").text();
            String[] parts = doc.select("#resumeLists > div.sider > div > div > div > div.approve_info.clearfix > p").text().split(" ");
            String title = Stream.of(parts).limit(parts.length - 1).collect(Collectors.joining());
            String name = parts[parts.length - 1];
            String avatarUrl = doc.select("#resumeLists > div.sider > div > div > div > div.img > a:nth-child(1) > img").attr("src");
            user.setName(name);
            user.setTitle(title);
            user.setNick(nick);
            user.setAvatarUrl(avatarUrl);
            Elements elements = doc.select("#resumeLists > div.maincontent > div > div.content.brjobs.topjobs > ul > li");
            for(Element element : elements) {
                Job job = new Job();
                String jobTitle = element.select("div.jobnote > a > b").text();
                String company = element.select("div.jobtitle > a:nth-child(2)").text();
                Matcher matcher = Pattern.compile("月薪：.*?(\\d+k\\s*~\\s*\\d+k)").matcher(element.html());
                String salary = "";
                if (matcher.find()) {
                    salary = matcher.group(1);
                }
                String address = element.select("div.jobtitle > a:nth-child(7)").text();
                job.setTitle(jobTitle);
                job.setAddress(address);
                job.setSalary(salary);
                job.setApartment(company);
                user.getJobs().add(job);
            }
        } catch (IOException e) {
            System.out.println("Get " + url + " failed.");
            e.printStackTrace();
        }
        return user;
    }
}
