package Sayalic.hack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "by",
        "descendants",
        "id",
        "kids",
        "score",
        "time",
        "title",
        "type",
        "url"
})
public class HackItem {
    private static HackItem defaultItem = new HackItem();
    static {
        defaultItem.setType(null);
    }
    public static HackItem getDefaultHackItem() {
        return defaultItem;
    }
    @JsonProperty("by")
    private String by;
    @JsonProperty("descendants")
    private Integer descendants;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("kids")
    private List<Integer> kids = new ArrayList<Integer>();
    @JsonProperty("score")
    private Integer score;
    @JsonProperty("time")
    private Integer time;
    @JsonProperty("title")
    private String title;
    @JsonProperty("type")
    private String type;
    @JsonProperty("url")
    private String url;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The by
     */
    @JsonProperty("by")
    public String getBy() {
        return by;
    }

    /**
     * @param by The by
     */
    @JsonProperty("by")
    public void setBy(String by) {
        this.by = by;
    }

    /**
     * @return The descendants
     */
    @JsonProperty("descendants")
    public Integer getDescendants() {
        return descendants;
    }

    /**
     * @param descendants The descendants
     */
    @JsonProperty("descendants")
    public void setDescendants(Integer descendants) {
        this.descendants = descendants;
    }

    /**
     * @return The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The kids
     */
    @JsonProperty("kids")
    public List<Integer> getKids() {
        return kids;
    }

    /**
     * @param kids The kids
     */
    @JsonProperty("kids")
    public void setKids(List<Integer> kids) {
        this.kids = kids;
    }

    /**
     * @return The score
     */
    @JsonProperty("score")
    public Integer getScore() {
        return score;
    }

    /**
     * @param score The score
     */
    @JsonProperty("score")
    public void setScore(Integer score) {
        this.score = score;
    }

    /**
     * @return The time
     */
    @JsonProperty("time")
    public Integer getTime() {
        return time;
    }

    /**
     * @param time The time
     */
    @JsonProperty("time")
    public void setTime(Integer time) {
        this.time = time;
    }

    /**
     * @return The title
     */
    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The url
     */
    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    /**
     * @param url The url
     */
    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}