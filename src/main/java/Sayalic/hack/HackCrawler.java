package Sayalic.hack;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.fluent.Request;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;
import java.util.stream.IntStream;

/**
 * Created by Sona on 9/19/16.
 */
public class HackCrawler {
    private static ObjectMapper mapper = new ObjectMapper();
    private static String getUrlResponse(String url) throws IOException {
        return Request.Get(url).execute().returnContent().asString();
    }

    private static String crawlItem(int id) {
        try {
            String json = getUrlResponse(String.format("https://hacker-news.firebaseio.com/v0/item/%d.json", id));
            if (json.contains("\"type\":\"story\"")) {
                return json;
            }
        } catch (IOException e) {
            System.out.println("crawl " + id + " failed");
        }
        return "";
    }

    public static void main(String[] args) throws FileNotFoundException {
        Logger logger = Logger.getLogger("main");
        PrintWriter printWriter = new PrintWriter("hack.json");
        for (int i = 0; i < 10000; i++) {
            logger.info("batch " + i);
            IntStream.range(12530671 - (i + 1) * 100, 12530671 - i * 100)
                    .parallel()
                    .mapToObj(HackCrawler::crawlItem).filter(x -> !x.equals(""))
                    .forEach(printWriter::println);
        }
    }
}
