package Sayalic;

import java.util.List;

/**
 * Created by Sona on 8/29/16.
 */
public class Job {
    private String referrerNick;
    private String referrerTitle;
    private String referrerName;
    private String referrerAvatarUrl;
    private String referrerUrl;
    private String apartment;
    private String salary;
    private String address;
    private String title;
    private String url;
    private String description;
    private String experience;
    private List<String> tags;

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getReferrerNick() {
        return referrerNick;
    }

    public void setReferrerNick(String referrerNick) {
        this.referrerNick = referrerNick;
    }

    public String getReferrerTitle() {
        return referrerTitle;
    }

    public void setReferrerTitle(String referrerTitle) {
        this.referrerTitle = referrerTitle;
    }

    public String getReferrerName() {
        return referrerName;
    }

    public void setReferrerName(String referrerName) {
        this.referrerName = referrerName;
    }

    public String getReferrerAvatarUrl() {
        return referrerAvatarUrl;
    }

    public void setReferrerAvatarUrl(String referrerAvatarUrl) {
        this.referrerAvatarUrl = referrerAvatarUrl;
    }

    public String getReferrerUrl() {
        return referrerUrl;
    }

    public void setReferrerUrl(String referrerUrl) {
        this.referrerUrl = referrerUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
